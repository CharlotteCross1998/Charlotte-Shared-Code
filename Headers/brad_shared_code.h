#ifndef BRAD_SHARED_CODE_H_INCLUDED
#define BRAD_SHARED_CODE_H_INCLUDED

typedef unsigned int uint;

//namespace Brad
//{
//	namespace Sort
//	{
		template <typename arr>
		void BubbleSort(arr array[], int size)
		{
			for(int i = 0; i < size - 1; ++i)
			{
				for(int j = 0; j < size - 1; j++)
				{
					if(array[j] > array[j+1])
					{
						int temp = array[j+1];
						array[j+1] = array[j];
						array[j] = temp;
					}
				}
			}
		} 
//	}

//	namespace Search
//	{
		template <typename arr, typename que>
		const int LinearSearch(arr array[], int arraySize, que query)
		{
			for(int i = 0; i < arraySize -1; ++i) if (array[i] == query) return i;
			return -1;
		}
//	}

//	namespace Time
//	{
		#include <chrono>
		
		class Stopwatch
		{
			private:
				const double NANOSECONDS = 1000000000.0;
				bool isRunning = false;
				bool hasRun = false;
				std::chrono::time_point<std::chrono::high_resolution_clock> t1 = std::chrono::high_resolution_clock::now();
				std::chrono::time_point<std::chrono::high_resolution_clock> t2 = std::chrono::high_resolution_clock::now();
			public:
				void Start() { isRunning = true; hasRun = false; t1 = std::chrono::high_resolution_clock::now(); } const
				void Stop() 
				{
					if(isRunning)
				   	{
						isRunning = false;
						hasRun = true;
						t2 = std::chrono::high_resolution_clock::now(); 
					}
					else std::cout << "Error stopping timer: Timer not running\n";
				} const
				//returns time in seconds
				const double GetTimeTaken() { if(hasRun) return std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count() / NANOSECONDS; else return -1;}
				//returns time in nanoseconds
				const double GetRawTimeTaken() { if(hasRun) return std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count(); else return -1;}
		};

		class Timer
		{
			private:
				Stopwatch t;
			public:
				Timer() { t.Start(); } const
				//returns time in seconds
				const double GetTimeTaken() { return t.GetTimeTaken(); }
				//returns time in nanoseconds
				const double GetRawTimeTaken() { return t.GetRawTimeTaken(); }
				const double Stop() { t.Stop(); return t.GetTimeTaken(); }
		};
//	}

//	namespace Numbers
//	{
		/*template <class T>
		bool isEven(T num) { return (num % 2 == 0); }
		bool isOdd(T num) { return (num % 2 == 1); }*/
//	}
//}

#endif // BRAD_SHARED_CODE_H_INCLUDED
